package com.hunt.users.users;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Phone {
	private String areaCode, prefix, trunk, extension;

	public Phone() {		
	}
	
	public Phone(String ac, String pf, String tr, String ext) {
		this.areaCode = ac;
		this.prefix = pf;
		this.trunk = tr;
		this.extension = ext;
	}
	
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getTrunk() {
		return trunk;
	}

	public void setTrunk(String trunk) {
		this.trunk = trunk;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public static final Pattern phonePattern =  Pattern.compile(".*(\\d{3}).*(\\d{3}).*(\\d{4}).*(\\d{1,4})?.*");

	public static void main(String[] arges) {
		Phone p = Phone.valueOf("608 798 3522");
		System.out.println(p);
	}
	
	
	public static Phone valueOf(String ph) {
		Matcher m = phonePattern.matcher(ph);		
		Phone p = new Phone(m.group(1), m.group(2), m.group(3), m.group(4));		
		return p;
	}
	
	public static List<Phone> parsePhones(List<String> phones) {
		List<Phone> result = new ArrayList<Phone>();
		for(String s : phones) {
			result.add(Phone.valueOf(s));
		}
		return result;
	}
}
