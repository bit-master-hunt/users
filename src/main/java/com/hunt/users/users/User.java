package com.hunt.users.users;

import java.util.Arrays;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class User {
	@Id	
	private String id;
	private UserProfile profile;		
	private List<String> roles;	
	@Indexed
	private String username;
	
	public User() {
		
	}
	
	private User(Builder b) {
		this.id = b.id;
		this.profile = b.profile;
		this.roles = b.roles;
		this.username = b.username;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public void setUsername(String userName) {
		this.username = userName;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public List<String> getRoles() {
		return roles;
	}
	
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
	public void setProfile(UserProfile profile) {
		this.profile = profile;
	}
	
	public UserProfile getProfile() {
		return this.profile;		
	}
	
	public static class Builder {
		private String id;
		private List<String> roles;
		private UserProfile profile;
		private String username;
		
		public Builder id(String id) {
			this.id = id;
			return this;
		}
		
		public Builder roles(List<String> roles) {
			this.roles = roles;
			return this;
		}		
		
		public Builder roles(String... roles) {
			this.roles = Arrays.asList(roles);
			return this;
		}
		
		public Builder username(String username) {
			this.username= username;
			return this;
		}
		
		public Builder profile(UserProfile profile) {
			this.profile = profile;
			return this;
		}
		
		public User build() {
			return new User(this);
		}
		
	}
}
