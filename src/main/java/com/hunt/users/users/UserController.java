package com.hunt.users.users;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Validated
@RequestMapping("/users")
public class UserController {
	@Autowired
	private UserServices userServices;

	@RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public User createUser(
			@Size(min=1) @NotNull @RequestParam String username, 
			@Size(min=1) @NotNull @RequestParam String password, 
			@Size(min=1) @NotNull @RequestParam String firstName, 
			@Size(min=1) @NotNull @RequestParam String lastName, 
			@Size(min=1) @NotNull @RequestParam List<String> emails,
			@Size(min=1) @NotNull @RequestParam List<String> phoneNumbers) {
		List<Phone> phones = Phone.parsePhones(phoneNumbers);
		return userServices.createUser(username, password, firstName, lastName, emails, phones);		
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
	@ResponseBody
	public List<User> allUsers() {
		return userServices.all();
	}
	
	@RequestMapping(value = "/{uid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
	@ResponseBody
	public User getUser(@PathVariable String uid) {
		return userServices.find(uid);
	}
	
	@RequestMapping(value = "/{uid}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE )
	@ResponseBody
	public List<User> deleteUser(@PathVariable String uid) {
		userServices.deleteUser(uid);
		return allUsers();
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
	@ResponseBody
	public User lookup(@RequestParam String username, @RequestParam String password) {
		return userServices.findByUsernameAndPassword(username, password);
	}
	
	
	@RequestMapping(value = "/{uid}/profile", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE )
	@ResponseBody
	public UserProfile updateUserProfile(@PathVariable String uid,
				@RequestParam(required = false) String firstName,
				@RequestParam(required = false) String lastName,
				@RequestParam(required = false) List<String> emails,
				@RequestParam(required = false) List<String> phoneNumbers ) {
		
		userServices.updateUserProfile(uid, firstName, lastName, emails, phoneNumbers);		
		return userServices.find(uid).getProfile();
	}
	
}
