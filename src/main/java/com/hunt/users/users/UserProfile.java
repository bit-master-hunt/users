package com.hunt.users.users;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserProfile {
	@JsonProperty("first")
	private String firstName;
	@JsonProperty("last")
	private String lastName;
	private List<String> emails;
	private List<Phone> phones;
	private String password;
	
	public UserProfile() {
		
	}
	
	private UserProfile(Builder b) {
		this.emails = b.emails;
		this.firstName = b.firstName;
		this.lastName = b.lastName;
		this.phones = b.phones;
		this.password = b.password;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public List<String> getEmails() {
		return emails;
	}
	public void setEmails(List<String> emails) {
		this.emails = emails;
	}
	public List<Phone> getPhones() {
		return phones;
	}
	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}
	
	public static class Builder {
		private String firstName;
		private String lastName;
		private List<String> emails;
		private List<Phone> phones;
		private String password;
		
		public Builder password(String password) {
			this.password = password;
			return this;
		}
		
		public Builder lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}
		
		public Builder firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}
		
		public Builder emails(List<String> emails) {
			this.emails = emails;
			return this;
		}
		
		public Builder phones(List<Phone> phones) {
			this.phones = phones;
			return this;
		}
		
		public UserProfile build() {
			return new UserProfile(this);
		}
		
	}
}
