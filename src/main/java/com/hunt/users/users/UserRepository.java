package com.hunt.users.users;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface UserRepository extends MongoRepository<User, String> {
	public User findByUsername(String username);
	
	@Query(value= "{ 'username' : ?0, 'profile.password' : ?1 }" )
	public User findByUsernameAndPassword(String username, String password);
}
