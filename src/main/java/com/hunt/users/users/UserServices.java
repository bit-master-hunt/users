package com.hunt.users.users;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServices {
	@Autowired
	private UserRepository userRepository;
	
	public List<User> all() {
		return userRepository.findAll();
	}
	
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}
	
	public User findByUsernameAndPassword(String username, String password) {
		return userRepository.findByUsernameAndPassword(username, password);
	}
	
	public User createUser(String userName, String password, String fn, String ln, List<String> emails, List<Phone> phones) {
		UserProfile profile = new UserProfile.Builder()
			.phones(phones)
			.emails(emails)
			.firstName(fn)
			.lastName(ln)
			.password(password)
			.build();
		
		User user = new User.Builder()
			.username(userName)
			.roles("USER")
			.profile(profile)
			.build();
		
		userRepository.insert(user);
		return userRepository.findByUsername(userName);
	}
	
	public void deleteUser(String userId) {
		userRepository.delete(userId);
	}
	
	public User find(String userid) {
		return userRepository.findOne(userid);
	}

	public void updateUserProfile(String uid, String firstName, String lastName, List<String> emails, List<String> phoneNumbers) {
		User user = userRepository.findOne(uid);		
	}
}
