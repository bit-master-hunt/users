  users = [{
      first: "Jim",
      last: "Johnson",
      email: "jj@gmail.com"
  }, {
      first: "Julie",
      last: "Jackson",
      email: "jj@yahoo.com"
  }, {
      first: "Johnny",
      last: "Joseph",
      email: "jj@rocketmail.com"
  }, {
      first: "Jemimah",
      last: "John",
      email: "jj@charter.net"
  }, {
      first: "Joanna",
      last: "Jablonski",
      email: "jj@facebook.com"
  }]

  toHtml = function(users) {
      var userNode = document.getElementById("users");
      // remove any existing users
      while (userNode.firstChild) {
          userNode.removeChild(userNode.firstChild);
      }

      // add users
      users.forEach(function(val, i) {
          var div = document.createElement('div');
          div.className = "well";
          var firstSpan = document.createElement('span');
          var lastSpan = document.createElement('span');
          var emailSpan = document.createElement('span');
          div.appendChild(firstSpan);
          div.appendChild(lastSpan);
          div.appendChild(emailSpan);
          firstSpan.appendChild(document.createTextNode(val.first));
          lastSpan.appendChild(document.createTextNode(val.last));
          emailSpan.appendChild(document.createTextNode(val.email));
          userNode.appendChild(div);
      });
  }