  var users = []
  getUsers = function() {
	  console.log('getting all users');
      $.ajax({
          url: '/users/users',
          type: 'get',
          dataType: 'json',
          success: function(response) {
        	  console.log(response);
              users = response;
              toHtml(users);
          },
          error: function(resonse) {
              alert("Could not load the user list");
          }
      });
  }
  
  deleteUser = function(uid) {
	  console.log('deleting a user: ' + uid);
	  $.ajax({
          url: '/users/users/' + uid,
          type: 'delete',
          dataType: 'json',
          success: function(response) {        	  
        	  users = response;
        	  toHtml(users);
          },
          error: function(resonse) {
              alert("Could not load the user list");
          }
      });
  }
  
  createUser = function() {
	  console.log('creating user');
	  var username = $('#usernameField').val();
	  var password = $('#passField').val();
	  var phone = $('#phoneNumberField').val();
	  var fn = $('#firstField').val();
	  var ln = $('#lastField').val();
	  var em = $('#emField').val();
	  $.ajax({
          url: '/users/users',
          type: 'post',
          data: { firstName : fn, lastName : ln, emails : em, username : username, password : password, phoneNumbers : phone},
          dataType: 'json',
          success: function(response) {
              newUser = response;
              console.log(newUser);
              getUsers();
          },
          error: function(resonse) {
              alert("Could not load the user list");
          }
      });
  }

  toHtml = function(users) {
      var userNode = document.getElementById("users");
      // remove any existing users
      while (userNode.firstChild) {
          userNode.removeChild(userNode.firstChild);
      }

      // add users
      users.forEach(function(val, i) {
          var div = document.createElement('div');
          div.className = "well";
          var firstSpan = document.createElement('span');
          var lastSpan = document.createElement('span');
          var emailSpan = document.createElement('span');
          var delSpan = document.createElement('span');
          div.appendChild(firstSpan);
          div.appendChild(lastSpan);
          div.appendChild(emailSpan);
          div.appendChild(delSpan);
          firstSpan.appendChild(document.createTextNode(val.profile.first));
          lastSpan.appendChild(document.createTextNode(val.profile.last));
          emailSpan.appendChild(document.createTextNode(val.profile.emails));
          delSpan.className="btn btn-xs btn-danger glyphicon glyphicon-trash pull-right";
          delSpan.addEventListener('click', function() { deleteUser(val.id); } );
          userNode.appendChild(div);
      });
  }